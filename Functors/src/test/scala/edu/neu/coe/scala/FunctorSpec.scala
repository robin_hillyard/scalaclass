package edu.neu.coe.scala

import org.scalatest.{ FlatSpec, Matchers }

/**
 * @author scalaprof
 */
class FunctorSpec extends FlatSpec with Matchers {

  "map" should "result in a list of string lengths" in {
    val x = FunctorExamples.sizes(List("a","bc","def", ""))
    x should be (List(1,2,3,0))
  }

  it should "result in an empty list if given an empty list" in {
    val x = FunctorExamples.sizes(List())
    x should be (List())
  }
  
  "flatMap" should "result in a list of characters" in {
    val x = FunctorExamples.chars(List("a","bc","def", ""))
    x should be (List('a','b','c','d','e','f'))
  }

  it should "result in an empty list if given an empty list" in {
    val x = FunctorExamples.chars(List())
    x should be (List())
  }
}