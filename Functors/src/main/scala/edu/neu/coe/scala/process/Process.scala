package edu.neu.coe.scala.process

/**
 * @author scalaprof
 */

trait Process[-I,+O] extends Function1[Stream[I],Stream[O]]{
  def apply(s: Stream[I]): Stream[O] = this match {
    case Halt() => Stream()
    case Emit(h,t) => h #:: t(s)
    case Await(recv) => s match {
      case h #:: t => recv(Some(h))(t)
      case z => recv(None)(z)
    }
  }
  def repeat: Process[I,O] = {
     def go(p: Process[I,O]): Process[I,O] = p match {
     case Halt() => go(this)
     case Await(recv) => Await { case None => recv(None); case i => go(recv(i)) }
     case Emit(h,t) => Emit(h, go(t))
     }
     go(this)
     }
}

case class Emit[I,O](head: O, tail: Process[I,O] = Halt[I,O]()) extends Process[I,O]

case class Halt[I,O]() extends Process[I,O]

case class Await[I,O](recv: Option[I] => Process[I,O]) extends Process[I,O]

object Process {
  def liftOne[I,O](f: I => O): Process[I,O] =
    Await {
      case Some(i) => Emit(f(i))
      case None => Halt()
    }
  

}