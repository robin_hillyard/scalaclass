package edu.neu.coe.scala

import scala.collection.GenTraversableOnce

/**
 *  From fpinscala p 188
 * 
 */
trait Functor[F[_]] {
  def map[A,B](fa: F[A])(f: A => B): F[B]
  def flatMap[A,B](fa: F[A])(f: A => GenTraversableOnce[B]): F[B]
}

object FunctorExamples {
  val listFunctor = new Functor[List] {
    def map[A,B](as: List[A])(f: A => B): List[B] = as map f
    def flatMap[A,B](as: List[A])(f: A => GenTraversableOnce[B]): List[B] = as flatMap f
  }
  
  def sizes(fa: List[CharSequence]) = listFunctor.map(fa)({x => x.length})
  def chars(fa: List[String]) = listFunctor.flatMap(fa)({x => x.toList})
  
  def main(args: Array[String]): Unit = {
    println(sizes(List("a","bc","def", "")))
    println(chars(List("a","bc","def", "")))
  }
}