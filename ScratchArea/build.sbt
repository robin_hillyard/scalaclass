name := "ScratchArea"

version := "1.0.0-SNAPSHOT"

val scalaVersion = "2.11.7"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
	"ch.qos.logback" % "logback-classic" % "1.0.0" % "runtime",
    "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
)
