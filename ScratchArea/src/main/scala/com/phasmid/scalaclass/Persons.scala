package com.phasmid.scalaclass

/**
 * @author robinhillyard
 */
case class Person(name: String)

case class Result(persons: Seq[Person])

object Persons {
def main(args: Array[String]): Unit = {
  val results = Array[Result] (Result(Array(Person("Robin Hillyard"))))
  val x = results.map(r => r.persons.filter(_.name=="Robin Hillyard"))
  println(x.toList)
}
}