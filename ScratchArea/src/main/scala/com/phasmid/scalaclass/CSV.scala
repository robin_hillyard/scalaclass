

/**
 * @author robinhillyard
 */
object CSV {
  def readCSV = {
  io.Source.fromInputStream(getClass.getResourceAsStream("/testData.csv"))
    .getLines()
    .map(_.split(",").map(_.trim.toDouble))
    .toArray
}
  
  def main(args: Array[String]): Unit = {
    val result = readCSV
    result map {x => x map {y => print(s"$y ")}; println}
  }
}