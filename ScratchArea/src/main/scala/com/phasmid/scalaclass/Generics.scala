package com.phasmid.scalaclass

import scala.collection.immutable.HashMap

/**
 * @author robinhillyard
 */
class Generics {
  private class SectionConversionMap
  extends Map[SectionSchema[_], ( Iterable[HtmlRow] ) => Option[Iterable[_]]] {
private type Value[Vt] = ( Iterable[HtmlRow] ) => Option[Iterable[Vt]]
private type SuperKV = (SectionSchema[_], Value[_])

def +[T](kv: (SectionSchema[T], Value[T])):
Map[SectionSchema[_], ( Iterable[HtmlRow] ) => Option[Iterable[_]]] = {
  this+(kv: SuperKV)
}
def get[T](key: SectionSchema[T]): Option[Value[T]] = {
  get(key) match {
    case v: Some[Value[T]] => v
    case _ => None
  }
}
}
  class SectionSchema[K]
  class HtmlRow
}