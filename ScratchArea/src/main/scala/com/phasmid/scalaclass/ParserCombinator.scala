package com.phasmid.scalaclass

import scala.language.higherKinds

/**
 * @author robinhillyard
 */
trait Parsers[ParseError, Parser[+_]] {
  def run[A](p :Parser[A])(input: String): Either[ParseError,A]
  def char(x: Char): Parser[Char]
  def string(s: String): Parser[String]
  def or[A](s1: Parser[A], s2: Parser[A]): Parser[A]
}

object ParserCombinator {
  
}