package com.phasmid.scalaclass

import org.scalatest.{ WordSpecLike, Matchers, BeforeAndAfterAll, Inside }

/**
 * @author robinhillyard
 */
class ParserCombinatorSpec extends WordSpecLike with Matchers with Inside {

  "Abracadabra" in {
    val pc = or(string("abra"), string("cadabra"))
    predicate.apply(MapCandidate("test", Map("x" -> "2"))) should matchPattern {
      case Right(true) =>
    }
    predicate.apply(MapCandidate("test", Map("x" -> "4"))) should matchPattern {
      case Right(false) =>
    }
  }


}
