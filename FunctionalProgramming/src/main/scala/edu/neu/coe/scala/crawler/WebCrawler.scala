package edu.neu.coe.scala.crawler

import java.net.URL
import scala.io.Source
import scala.util._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import edu.neu.coe.scala.MonadOps

/**
 * @author scalaprof
 */
object WebCrawler extends App { 

  def getURLContent(u: URL): Future[String] = {
    for {
      source <- Future(Source.fromURL(u))
    } yield source mkString
  }
  
  def countWords(st: Try[String]) =
    for { gD <- st; n <- Try(HTMLParser.parse(gD)); gN <- Try(n.text); l <- Try(gN.split(" ").length) } yield l
  
  def wget(u: URL): Future[Seq[URL]] = {
    def getLinks(g: String): Seq[URL] = for (nsA <- HTMLParser.parse(g) \\ "a"; nsH <- nsA \ "@href"; nH <- nsH.apply(0); ut = new URL(u,nH.toString)) yield ut
    println(s"wget: $u")
    for { g <- getURLContent(u); r <- Future(getLinks(g)) } yield r
  }

  /*
   * wget: if any of the input URLs fail, then this method fails.
   * CONSIDER At some point, we may separate out the successes and failures
   */
  def wget(us: Seq[URL]): Future[Seq[Either[Throwable,Seq[URL]]]] = {
    val x = for { u <- us } yield wget(u)
    val y = MonadOps.sequence(x)
    Future.sequence(y)
  }
    
  def filterAndFlatten(esf: Future[Seq[Either[Throwable,Seq[URL]]]]): Future[Seq[URL]] = {
    def filter(uses: Seq[Either[Throwable, Seq[URL]]]): Seq[URL] = {
      val uses2 = for { use <- uses; if (use match {case Left(x) => println(x); false; case _ => true})} yield use
      val uss = for { use <- uses2; uso = use.right.toOption; us <- uso } yield us
      uss flatten
    }
    for { es <- esf; e = filter(es) } yield e
  }
    
  def crawler(args: Seq[URL]): Future[Seq[URL]] = {
		def inner(urls: Seq[URL], depth: Int, accum: Seq[URL]): Future[Seq[URL]] =
		  if ( depth>0 )
			  for (us <- filterAndFlatten(wget(urls)); r <- inner(us,depth-1,accum ++: urls)) yield r
			else
			  Future.successful(accum)
    inner(args, 2, List())
  }
  
println(s"web reader: ${args.toList}")
  val urls = for ( arg <- args toList ) yield Try(new URL(arg))
  val s = MonadOps.sequence(urls)
  s match {
    case Success(z) => {
      println(s"invoking crawler on $z")
      val f = crawler(z)
      Await.ready(f, Duration("60 second"))
      for ( x <- f) println(s"Links: $x")
    }
    case Failure(z) => println(s"failure: $z")
  }
}
