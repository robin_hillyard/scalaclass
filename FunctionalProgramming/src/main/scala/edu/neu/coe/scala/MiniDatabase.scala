package edu.neu.coe.scala.minidatabase

import scala.io.Source
import scala.util._

/**
 * @author scalaprof
 */
object MiniDatabase {
  def load(filename: String) = {
    val src = Source.fromFile(filename)
    val database = src.getLines.toList.map(e => Entry(e.split(",")))
    val result = database.toSeq
    src.close
    result
  }
  
  def measure(height: Height) = height match {
    case Height(8,_) => "giant"
    case Height(7,_) => "very tall"
    case Height(6,_) => "tall"
    case Height(5,_) => "normal"
    case Height(_,_) => "short"
  }
  
  def main(args: Array[String]): Unit = {
    if (args.length>0) {
      val db = load(args(0))
      print(db)
    }
  }
}

//case class Entry(name: Name, social: Social, dob: Date, height: Height, weight: Int)
case class Entry(height: Height)

case class Height(feet: Int, in: Int) {
  def inches = feet*12+in
}

object Entry {
//  def apply(name: String, social: String, dob: String, height: String, weight: String): Entry =
//    Entry(Name(name),Social(social),Date(dob),Height(height),weight.toInt)
//  def apply(entry: Seq[String]): Entry = apply(entry(0),entry(1),entry(2),entry(3),entry(4))
  def apply(height: String): Entry =
    Entry(Height(height))
  def apply(entry: Seq[String]): Entry = apply(entry(3))
}

object Height {
  val rHeightFtIn = """^\s*(\d+)\s*(?:ft|\')(\s*(\d+)\s*(?:in|\"))?\s*$""".r
  def apply(ft: String, in: String) = new Height(ft.toInt,in.toInt)
  def apply(ft: Int) = new Height(ft,0)
  def apply(height: String): Height = height match {
    case rHeightFtIn(ft,_,in) => Height(ft,in)
    case rHeightFtIn(ft) => Height(ft.toInt)
    case _ => throw new IllegalArgumentException(height)
  }
}

//case class Name(first: String, middle: String, last: String)

//case class Social(are: Int, group: Int, serial: Int)

//case class Date(year: Int, month: Int, day: Int)

//object Name {
//  val rName="""^(\w+)\s+((.*)\s+)?(\w+)$""".r
//  def parse(name: String): Try[Name] = name match{
//    case rName(first,_,middle,last) => Success(Name(first,middle,last))
//    case rName(first,last) => Success(Name(first,"",last))
//    case _ => Failure(new IllegalArgumentException(name))
//  }
//}

//object Date {
//  val dateTimeISOFull = """^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})(?:\.{0,1})(\d{0,})(Z|([\-+])(\d{2})(?::{0,1})(\d{2}))?)?)?)?$""".r
//  val dateISO = """^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})$""".r
//  val dateUS = """^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$""".r
//  def apply(year: String, month: String, day: String) = ???  
//  def apply(date: String): Date = date match {
//    case dateTimeISOFull(yr,mo,dy,hr,mn,sec,t,_,zh,zm) => Date(yr,mo,dy)
//    case dateISO(yr,mo,dy) => Date(yr,mo,dy)
//    case dateUS(mo,dy,yr) => Date(yr,mo,dy)
//  }
//}

//object Social {
//  def apply(ssn: String): Social = ???
//}