package exam


/**
 * @author scalaprof
 */
case class DNA(x: List[Base]) {
  def +(b: Base) = DNA(b+:x)
  def ++(d: DNA) = DNA(d.x++x)
  def zip(d: DNA) = x zip d.x
  def euclidean(d: DNA) = (for ((a,b) <- zip(d)) yield DNA.dist(a,b)) reduceLeft{_+_}
  def basePairs = x.length
  override def toString = x.reverse.foldLeft(""){case (s,d) => s+d.toString}
}
trait Base {
  def pair: Base
}
case object Cytosine extends Base {
  def pair = Guanine
  override def toString = "C"
}
case object Guanine extends Base {
  def pair = Cytosine
  override def toString = "G"
}
case object Adenine extends Base {
  def pair = Thymine
  override def toString = "A"
}
case object Thymine extends Base {
  def pair = Adenine
  override def toString = "T"
}
case class Invalid(x: Char) extends Base {
  def pair = null
  override def toString = s"<Invalid: $x>"
}
object Base {
  def apply(x: Char) = x match {
    case 'G' => Guanine
    case 'C' => Cytosine
    case 'A' => Adenine
    case 'T' => Thymine
    case _ => Invalid(x)
  }
}

object DNA {
  def apply(): DNA = apply(List())
  def apply(s: String): DNA = s.toSeq.foldLeft(DNA())({_+Base(_)})
  def dist(a: Base, b: Base) = if (a==b) 0 else 1
}
