package edu.neu.coe.scala

// This is the answer for Final Exam question 5

object BiMap {
  println("Welcome to the BiMap worksheet")       //> Welcome to the BiMap worksheet
  
  def invert[A,B](x: Map[B,A]): Map[A,B] = x map {_.swap}
                                                  //> invert: [A, B](x: Map[B,A])Map[A,B]
  
  val x = Map("a"->1, "b"->2)                     //> x  : scala.collection.immutable.Map[String,Int] = Map(a -> 1, b -> 2)
  invert(x)                                       //> res0: Map[Int,String] = Map(1 -> a, 2 -> b)
  
  val f = invert[Int,String] _                    //> f  : Map[String,Int] => Map[Int,String] = <function1>
  f(Map("a"->1, "b"->2))                          //> res1: Map[Int,String] = Map(1 -> a, 2 -> b)
}