package edu.neu.coe.scala

object Box {
  println("Welcome to the Box worksheet")         //> Welcome to the Box worksheet
  trait Box[+A] {
     def insert[B>:A](x: B): Box[B]
     def peek: A
     }
  
  class Book[+A](list: List[A]) extends Box[A] {
     def insert[B>:A](x: B): Box[B] = new Book(x::list)
     def peek: A = list.head
     override def toString = s"box of ${list.size} elements"
     }

  val book = new Book[String](List())             //> book  : edu.neu.coe.scala.Box.Book[String] = box of 0 elements
  val b = book.insert("A").insert("B")            //> b  : edu.neu.coe.scala.Box.Box[String] = box of 2 elements
  val c = b.insert("C".asInstanceOf[CharSequence])//> c  : edu.neu.coe.scala.Box.Box[CharSequence] = box of 3 elements
  
  val book2 = new Book[CharSequence](List())      //> book2  : edu.neu.coe.scala.Box.Book[CharSequence] = box of 0 elements
  val x: Any = "x"                                //> x  : Any = x
  val p = book2.insert(x)                         //> p  : edu.neu.coe.scala.Box.Box[Any] = box of 1 elements
  p.peek                                          //> res0: Any = x
}