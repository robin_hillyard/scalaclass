package edu.neu.coe.scala.numerics

trait LazyMonad[F[_],T] {
  def initial: F[T]
  def transform: T=>T
  def get: F[T]
  def unit[U](u: => U): LazyMonad[F,U]
  def flatMap[U](f: T=>LazyMonad[F,U]): LazyMonad[F,U]
  def map[U](f: T=>U): LazyMonad[F,U] = flatMap[U]{x: T => unit(f(x))}
}